% Algorithm to the local alingment based on the Smith–Waterman algorithm.

close all 
clear all 

% Alternative sequences to make the easiest alingment.  
seqA = 'AAACGTAAA';
seqB= 'CGT';

%  Selection for the sequences in fasta format. 
% [files]=uigetfile({'*.fasta;','All Fasta Files'},'Select the Files', 'Multiselect', 'on');
% 
% file1a = files{1,1};
% file2a = strrep(file1a,'fasta', 'txt'); 
% 
% % The files has to be in the main directory of Matlab. 
% copyfile(file1a,file2a)
% fidp = fopen(file2a); 
% Cab = textscan(fidp, '%s', 'delimiter', '\n');
% identificator = Cab{1}{1}; 
% sequence = strcat(Cab{1}{2:size(Cab{1,1}, 1),:});  
% sa = struct('Identificator', identificator, 'Sequence', sequence);
% seqA = sa.Sequence; 
% 
% 
% file1b = files{1,2}; 
% file2b = strrep(file1b,'fasta', 'txt'); 
% copyfile(file1b,file2b)
% fidpb = fopen(file2b); 
% Cab2 = textscan(fidpb, '%s', 'delimiter', '\n');
% identificator2 = Cab2{1}{1}; 
% sequence2 = strcat(Cab2{1}{2:size(Cab2{1,1}, 1),:});  
% sa2 = struct('Identificator', identificator2, 'Sequence', sequence2);
% seqB = sa2.Sequence; 

% Addition of the empty spaces for the sequences at the beginning. 
emptySpace = [' ']; 
sequenceAplusE = [emptySpace seqA];
sequenceBplusE = [emptySpace seqB];

% % Portion to make the two sequences equal. 
LengthSequenceA = length(sequenceAplusE); 
LengthSequenceB = length(sequenceBplusE); 

% Ask for the parameters of the scoring (-3, 3, 2). 
scoreIfequal = input('Specify the score when ai = bi:  '); 
scoreIfdifferent = input('Specify the score when ai != bi:  '); 
scoreWhengap = input('Specify the value for the gap penalty:  '); 

% Begining for the F matrix and the fillment. 
ScoreMatrixH = zeros(LengthSequenceB, LengthSequenceA); 
tic 
for k = 2:LengthSequenceA % k goes to the columns and h to the rows. 
    for h = 2:LengthSequenceB
          match = ScoreMatrixH(h-1,k-1) + ScoreDetermination(sequenceAplusE(k), sequenceBplusE(h), scoreIfequal, scoreIfdifferent); 
          delete = ScoreMatrixH(h-1,k) - scoreWhengap; 
          insert = ScoreMatrixH(h,k-1) - scoreWhengap;
          ScoreMatrixH(h,k) = max(max(match, delete), insert);
          if ScoreMatrixH(h,k) < 0 
              ScoreMatrixH(h,k) = 0; 
          end 
    end 
end 
toc

imagesc(ScoreMatrixH)
xlabel('Sequence 1'); 
ylabel('Sequence 2'); 
title('Scoring Matrix H'); 
colorbar
hold on 
saveas(gcf, 'ScoreMap.png')

%%  Determination of the best aligment according to the previous scores. 

AlignmentA = ''; 
AlignmentB = ''; 

scoreHmax = max(max(ScoreMatrixH)); 
indexMaxScore = (ScoreMatrixH == max(max(ScoreMatrixH))); 
[ni, nj] = find(indexMaxScore); 
AlignmentA = strcat(sequenceAplusE(nj), AlignmentA);
AlignmentB = strcat(sequenceBplusE(ni), AlignmentB); 

i = ni; 
j = nj; 

tic
while (i > 1 && j > 1)
    
    if (j == 2 || i == 2)
           break 
    end 
            
    % Cotinuacion of the traceback when the indexes are different of 2. 
    if (i > 0 && j > 0 && ScoreMatrixH(i,j) == ScoreMatrixH(i-1,j-1) + ScoreDetermination(seqA(j-1), seqB(i-1), scoreIfequal, scoreIfdifferent))
        AlignmentA = strcat(sequenceAplusE(j-1),AlignmentA);
        AlignmentB = strcat(sequenceBplusE(i-1), AlignmentB);
        i = i - 1; 
        j  = j -1; 
        
    elseif (i > 0 && ScoreMatrixH(i,j) < ScoreMatrixH(i-1,j) + scoreWhengap)
            AlignmentA = strcat(sequenceAplusE(j-1), AlignmentA); 
            AlignmentB = strcat('-',AlignmentB); 
            i = i - 1; 
        
     else 
            AlignmentA = strcat('-',AlignmentA); 
            AlignmentB = strcat(sequenceBplusE(i-1),AlignmentB); 
            j = j -1; 
 
    end 

%  Alterntative method 1. 
%     if (ScoreMatrixH(i-1, j) > ScoreMatrixH(i-1,j-1) && ScoreMatrixH(i-1, j) >= ScoreMatrixH(i, j-1))
%         AlignmentA = strcat('-', AlignmentA);
%         AlignmentB = strcat(sequenceBplusE(i-1), AlignmentB);
%         i = i-1; 
%         
%     elseif (ScoreMatrixH(i-1,j-1) > ScoreMatrixH(i-1,j) && ScoreMatrixH(i-1,j-1) >= ScoreMatrixH(i, j-1))
%         AlignmentA = strcat(sequenceAplusE(j-1), AlignmentA);
%         AlignmentB = strcat(sequenceBplusE(i-1), AlignmentB); 
%         i = i-1; 
%         j = j-1; 
%         
%     elseif (ScoreMatrixH(i, j-1) > ScoreMatrixH(i-1, j-1) && ScoreMatrixH(i,j-1) >= ScoreMatrixH (i-1,j))
%         AlignmentA = strcat(sequenceAplusE(j-1), AlignmentA);
%         AlignmentB = strcat('-', AlignmentB); 
%         j = j-1; 
%         
%     elseif (ScoreMatrixH(i-1,j-1) == ScoreMatrixH(i-1,j) || ScoreMatrixH(i-1,j-1) == ScoreMatrixH(i,j-1) || ScoreMatrixH(i-1,j) == ScoreMatrixH(i,j-1))  
%         AlignmentA = strcat(sequenceAplusE(j-1), AlignmentA);
%         AlignmentB = strcat(sequenceBplusE(i-1), AlignmentB); 
%         i = i-1; 
%         j = j-1; 
        
%  Alterntative method 2. 
%      if (ScoreMatrixH(i-1, j) > ScoreMatrixH(i-1,j-1) && ScoreMatrixH(i-1, j-1) ~= 0)
%         AlignmentA = strcat('-', AlignmentA);
%         AlignmentB = strcat(sequenceBplusE(i-1), AlignmentB);
%         i = i-1; 
%         
%     elseif (ScoreMatrixH(i-1,j) < ScoreMatrixH(i-1,j-1))
%         AlignmentA = strcat(sequenceAplusE(j-1), AlignmentA);
%         AlignmentB = strcat(sequenceBplusE(i-1), AlignmentB); 
%         i = i-1; 
%         j = j-1; 
%         
%      elseif (ScoreMatrixH(i,j-1) > ScoreMatrixH(i-1, j-1) && ScoreMatrixH(i-1, j-1) >= ScoreMatrixH(i-1,j))
%         AlignmentA = strcat(sequenceAplusE(j-1), AlignmentA);
%         AlignmentB = strcat('-', AlignmentB); 
%         j = j-1; 
        
%      elseif (ScoreMatrixH(i-1,j-1) == 0)
%         break
%         
%     end 
%         
end    
toc        


% Print the alingment in the command window. 
fprintf('The best alignment is:  \n')
fprintf('%s\n',AlignmentA)
fprintf('%s\n',AlignmentB)

% Final score Evaluation
scoreBysum = 0;
controlSum = zeros(1,length(AlignmentA));
minLength = min(length(AlignmentA), length(AlignmentB));
for l =1: minLength
        if(AlignmentA(l) == AlignmentB(l))
            scoreBysum = scoreBysum + scoreIfequal; 
            controlSum(l) = scoreBysum; 
            l = l+1; 
            
        elseif (AlignmentA(l) ~= AlignmentB(l) && AlignmentA(l) ~= '-' && AlignmentB(l) ~= '-')
            scoreBysum = scoreBysum + scoreIfdifferent; 
             controlSum(l) = scoreBysum; 
            l = l+1; 
            
        elseif (AlignmentA(l) == '-' || AlignmentB (l) == '-')
            scoreBysum = scoreBysum - scoreWhengap; 
             controlSum(l) = scoreBysum; 
            l = l+1; 
        end 
end 
 
% Determination for the identity and gaps. 
identity = 0;
gaps = 0; 
minLength2 = min(length(AlignmentA), length(AlignmentB));
for i = 1:minLength2
        if (AlignmentA(i)  == AlignmentB(i))
            identity = identity + 1;
            i = i +1; 
        elseif (AlignmentA(i) == '-' || AlignmentB (i) == '-')
            gaps = gaps + 1; 
            i = i +1; 
        end 
end 

% Creation for the correct fasta file with the general information. 
titles = {'# 1:  ','# 2:  ', '# Mode: Distance', '# Match: ', '# Mismatch: ', '# Gap: ', '# Score Max: ','# Score Calculated: ' , '# Length: ', '# Identity: ', '# Gaps: ', '# Alignment 1 is: ', '# Alignment 2 is: '};
elements = {seqA,seqB,' ', int2str(scoreIfequal),int2str(scoreIfdifferent), int2str(scoreWhengap),int2str(scoreHmax), int2str(scoreBysum), int2str(LengthSequenceA), int2str(identity), int2str(gaps), AlignmentA, AlignmentB};
fid = fopen('BestAlignment.fasta','w');
for row = 1:length(titles)
    fprintf(fid, '%s ',titles{row});
    fprintf(fid, '%s\n', elements{row});
end 
fclose(fid); 